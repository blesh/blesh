package com.hooshnahad.blesh.bleCore.homeDevices.Devices;

/**
 * Created by Freez on 7/23/2016.
 */
public enum DeviceType {
    //DPST = Double Pole Switch
    DOOR(0),DPST(1),COOLER(2),MOTION(3),PLUG(4);
    private final int value;
    private DeviceType(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
