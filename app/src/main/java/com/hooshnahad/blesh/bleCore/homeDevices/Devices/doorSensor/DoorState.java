package com.hooshnahad.blesh.bleCore.homeDevices.Devices.doorSensor;

/**
 * Created by Freez on 7/23/2016.
 */
public enum DoorState {
    OPEN(1),CLOSE(0);
    private final int value;
    private DoorState(int value){
        this.value = value;
    }
    public int getValue() {
        return value;
    }

}
