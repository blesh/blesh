package com.hooshnahad.blesh.bleCore.homeDevices.Devices.evaporativeCooler;

/**
 * Created by Freez on 7/23/2016.
 */
public enum EngineState {
    OFF(0),SLOW(1),FAST(2);
    private final int value;
    private EngineState(int value){
        this.value = value;
    }
}
