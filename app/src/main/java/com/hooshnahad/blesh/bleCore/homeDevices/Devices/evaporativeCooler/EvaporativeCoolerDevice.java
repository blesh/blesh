package com.hooshnahad.blesh.bleCore.homeDevices.Devices.evaporativeCooler;

import com.hooshnahad.blesh.bleCore.homeDevices.Devices.BleDevice;
import com.hooshnahad.blesh.bleCore.homeDevices.Devices.DeviceType;
import com.hooshnahad.blesh.bleCore.homeDevices.bleManager.BleConnectionState;

/**
 * Created by Freez on 7/20/2016.
 */
public class EvaporativeCoolerDevice extends BleDevice implements BleDevice.BleConnectionListener,BleDevice.BleRssiListener {

    EvaporativeCoolerChangeListener mEvaporativeCoolerChangeListener;
    PompState pompState;
    EngineState engineState;


    public EvaporativeCoolerDevice(String name,String address) {
        init(name,address);
    }

    public EvaporativeCoolerDevice(String name,String address,EvaporativeCoolerChangeListener listener) {
        this.mEvaporativeCoolerChangeListener = listener;
        init(name,address);
    }
    public void init(String name,String address){
        this.setDeviceType(DeviceType.COOLER);
        this.setName(name);
        this.setAddress(address);
        this.setBleRssiListener(this);
        this.setBleRssiListener(this);
        setPompState(PompState.OFF);
        setEngineState(EngineState.OFF);
    }

    public interface EvaporativeCoolerChangeListener{
        void evaporativeCoolerStateChange(String address,PompState pompState,EngineState engineState);
        void evaporativeCoolerRssiChange(String address,int rssi);
        void evaporativeCoolerConnectionChange(String address,BleConnectionState connectionState);
    }

    public PompState getPompState() {
        return pompState;
    }


    public void setEvaporativeCoolerChangeListener(EvaporativeCoolerChangeListener mEvaporativeCoolerChangeListener) {
        this.mEvaporativeCoolerChangeListener = mEvaporativeCoolerChangeListener;
    }

    public void setPompState(PompState pompState) {
        this.pompState = pompState;
        if(mEvaporativeCoolerChangeListener!=null)
            mEvaporativeCoolerChangeListener.evaporativeCoolerStateChange(getAddress(),getPompState(),getEngineState());
    }

    public EngineState getEngineState() {
        return engineState;
    }

    public void setEngineState(EngineState engineState) {
        this.engineState = engineState;
        if(mEvaporativeCoolerChangeListener!=null)
            mEvaporativeCoolerChangeListener.evaporativeCoolerStateChange(getAddress(),getPompState(),getEngineState());
    }

    @Override
    public void onBleConnectionChanged(String address, BleConnectionState connectionState) {
        if(address.equals(this.getAddress()))
            mEvaporativeCoolerChangeListener.evaporativeCoolerConnectionChange(address,connectionState);
    }
    @Override
    public void onChangeRssi(String address, int rssi) {
        if(address.equals(this.getAddress()))
            mEvaporativeCoolerChangeListener.evaporativeCoolerRssiChange(address, rssi);
    }
    public void tempTogglePomp(){
        if(getPompState()==PompState.OFF){
            setPompState(PompState.ON);
        }else
            setPompState(PompState.OFF);
    }
    public void tempToggleEngine(){
        if(getEngineState()==EngineState.OFF){
            setEngineState(EngineState.SLOW);
        }else if (getEngineState()==EngineState.SLOW)
            setEngineState(EngineState.FAST);
        else
            setEngineState(EngineState.OFF);
    }
}
