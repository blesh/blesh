package com.hooshnahad.blesh.bleCore.homeDevices.bleManager;

/**
 * Created by Freez on 7/20/2016.
 */
public enum  BleConnectionState {
    DISCONNECTED(0),CONNECTING(1),CONNECTED(2);
    private final int value;
    private BleConnectionState(int value){
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}
