package com.hooshnahad.blesh.bleCore.homeDevices.Devices.doublePoleSwitch;

/**
 * Created by Freez on 7/23/2016.
 */
public enum PoleSwitchState {
    OFF(0),DONTCARE(1),ON(2);

    private final int value;
    private PoleSwitchState(int value){
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}
