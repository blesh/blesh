package com.hooshnahad.blesh.bleCore.homeDevices.Devices.evaporativeCooler;

/**
 * Created by Freez on 7/23/2016.
 */
public enum  PompState {
    OFF(0),ON(1);
    private final int value;
    private PompState(int value){
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}
