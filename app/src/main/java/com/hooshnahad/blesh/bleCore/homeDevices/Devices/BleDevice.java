package com.hooshnahad.blesh.bleCore.homeDevices.Devices;

import com.hooshnahad.blesh.bleCore.homeDevices.bleManager.BleConnectionState;

/**
 * Created by Freez on 7/20/2016.
 */
public abstract class BleDevice {

    String name;
    String address;
    int rssi;
    DeviceType deviceType;
    BleConnectionState connectionState;
    private BleConnectionListener mBleConnectionListener;
    private BleRssiListener mBleRssiListener;

    public  boolean indicate(){
        // TODO: 7/20/2016  
        return false;
    }


    public boolean connect(){
        // TODO: 7/20/2016  
        return false;
    }
    public boolean disconnect(){
        // TODO: 7/20/2016  
        return false;
    }
    public BleConnectionState getConnectionState() {
        return connectionState;
    }

    public void setConnectionState(BleConnectionState connectionState) {
        this.connectionState = connectionState;
        mBleConnectionListener.onBleConnectionChanged(address,connectionState);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
        mBleRssiListener.onChangeRssi(address,rssi);
    }

    public interface BleConnectionListener{
        void onBleConnectionChanged(String address, BleConnectionState connectionState);
    }
    public interface BleRssiListener{
        void onChangeRssi(String address ,int rssi);
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }


    protected void setBleConnectionListener(BleConnectionListener mBleConnectionListener) {
        this.mBleConnectionListener = mBleConnectionListener;
    }


    protected void setBleRssiListener(BleRssiListener mBleRssiListener) {
        this.mBleRssiListener = mBleRssiListener;
    }
}
