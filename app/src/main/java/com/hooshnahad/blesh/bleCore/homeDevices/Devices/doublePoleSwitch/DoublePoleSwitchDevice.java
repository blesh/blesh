package com.hooshnahad.blesh.bleCore.homeDevices.Devices.doublePoleSwitch;


import com.hooshnahad.blesh.bleCore.homeDevices.Devices.BleDevice;
import com.hooshnahad.blesh.bleCore.homeDevices.Devices.DeviceType;
import com.hooshnahad.blesh.bleCore.homeDevices.bleManager.BleConnectionState;

/**
 * Created by Freez on 7/20/2016.
 */
public class DoublePoleSwitchDevice extends BleDevice implements BleDevice.BleConnectionListener,BleDevice.BleRssiListener{

    DoublePoleSwitchChangeListener mDoublePoleSwitchListener;
    PoleSwitchState firstPoleState;
    PoleSwitchState secondPoleState;
    double firstPolePowerConsumption;
    double secondPolePowerConsumption;


    public DoublePoleSwitchDevice(String name, String address) {
        init(name,address);
    }
    public DoublePoleSwitchDevice(String name, String address, DoublePoleSwitchChangeListener listener) {
        this.mDoublePoleSwitchListener = listener;
        init(name, address);
    }
    private void init(String name,String address){
        this.setDeviceType(DeviceType.DPST);
        this.setName(name);
        this.setAddress(address);
        this.setBleConnectionListener(this);
        this.setBleRssiListener(this);
        setFirstPoleState(PoleSwitchState.OFF);
        setFirstPoleState(PoleSwitchState.ON);
        setFirstPolePowerConsumption(0.0);
        setSecondPolePowerConsumption(0.0);

    }

    @Override
    public void onBleConnectionChanged(String address, BleConnectionState connectionState) {
        if(address.equals(this.getAddress()))
            mDoublePoleSwitchListener.onDoublePoleSwitchConnectionChange(address, connectionState);
    }

    @Override
    public void onChangeRssi(String address, int rssi) {
        if(address.equals(this.getAddress()))
            mDoublePoleSwitchListener.onDoublePoleSwitchRssiChange(this.getAddress(), rssi);
    }
    public interface DoublePoleSwitchChangeListener{
        public void onDoublePoleSwitchStateChange(String address, PoleSwitchState poleSwitchState1, PoleSwitchState poleSwitchState2);
        public void onDoublePoleSwitchConnectionChange(String address, BleConnectionState connectionState);
        public void onDoublePoleSwitchRssiChange(String address, int rssi);
        public void onDoublePoleSwitchPowerConsumptionChange(String address, double powerConsumption1, double powerConsumption2);
    }

    public PoleSwitchState getFirstPoleState() {
        return firstPoleState;
    }

    public void setFirstPoleState(PoleSwitchState firstPoleState) {
        this.firstPoleState = firstPoleState;
        if(mDoublePoleSwitchListener!=null)
            mDoublePoleSwitchListener.onDoublePoleSwitchStateChange(getAddress(),getFirstPoleState(),getSecondPoleState());
    }

    public PoleSwitchState getSecondPoleState() {
        return secondPoleState;
    }

    public void setSecondPoleState(PoleSwitchState secondPoleState) {
        this.secondPoleState = secondPoleState;
        if(mDoublePoleSwitchListener!=null)
            mDoublePoleSwitchListener.onDoublePoleSwitchStateChange(getAddress(),getFirstPoleState(),getSecondPoleState());
    }

    public double getFirstPolePowerConsumption() {
        return firstPolePowerConsumption;
    }

    public void setFirstPolePowerConsumption(double firstPowerConsumption) {
        this.firstPolePowerConsumption = firstPowerConsumption;
    }

    public double getSecondPolePowerConsumption() {
        return secondPolePowerConsumption;
    }

    public void setSecondPolePowerConsumption(double secondPowerConsumption) {
        this.secondPolePowerConsumption = secondPowerConsumption;
    }

    public void setDoublePoleSwitchListener(DoublePoleSwitchChangeListener mDoublePoleSwitchListener) {
        this.mDoublePoleSwitchListener = mDoublePoleSwitchListener;
    }
    public void toggleFirstPol(){
        if(getFirstPoleState()==PoleSwitchState.OFF){
            setFirstPoleState(PoleSwitchState.ON);
        }else
            setFirstPoleState(PoleSwitchState.OFF);
    }
    public void toggleSecondPol(){
        if(getSecondPoleState()==PoleSwitchState.OFF) {
            setSecondPoleState(PoleSwitchState.ON);
        }else
            setSecondPoleState(PoleSwitchState.OFF);
    }
}
