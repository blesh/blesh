package com.hooshnahad.blesh.bleCore.homeDevices.Devices.plug;

import com.hooshnahad.blesh.bleCore.homeDevices.Devices.BleDevice;
import com.hooshnahad.blesh.bleCore.homeDevices.Devices.DeviceType;
import com.hooshnahad.blesh.bleCore.homeDevices.bleManager.BleConnectionState;

/**
 * Created by Freez on 7/20/2016.
 */
public class PlugDevice extends BleDevice implements BleDevice.BleConnectionListener,BleDevice.BleRssiListener{

    PlugState state;
    double powerConsumption;
    PlugChangeListener mPlugChangeListener;

    public PlugDevice(String name,String address) {
        init(name,address);
    }

    public PlugDevice(String name,String address,PlugChangeListener mPlugChangeListener) {
        this.mPlugChangeListener = mPlugChangeListener;
        init(name,address);
    }
    public void init(String name,String address){
        this.setDeviceType(DeviceType.PLUG);
        this.setName(name);
        this.setAddress(address);
        this.setBleConnectionListener(this);
        this.setBleRssiListener(this);
        this.setState(PlugState.OFF);
        setPowerConsumption(0.0);

    }

    public interface PlugChangeListener{
        void onPlugStateChange(String address,PlugState state);
        void onPlugConnectionChange(String address,BleConnectionState connectionState);
        void onPlugRssiChange(String address,int rssi);

    }
    @Override
    public void onBleConnectionChanged(String address, BleConnectionState connectionState) {
        if(address.equals(this.getAddress()))
            mPlugChangeListener.onPlugConnectionChange(address,connectionState);
    }

    @Override
    public void onChangeRssi(String address, int rssi) {
        if(address.equals(this.getAddress()))
            mPlugChangeListener.onPlugRssiChange(address, rssi);

    }

    public PlugState getState() {
        return state;
    }

    public void setState(PlugState state) {
        this.state = state;
        if(mPlugChangeListener!=null)
            mPlugChangeListener.onPlugStateChange(this.getAddress(),state);
    }

    public double getPowerConsumption() {
        return powerConsumption;
    }

    public void setPowerConsumption(double powerConsumption) {
        this.powerConsumption = powerConsumption;
    }


    public void setPlugChangeListener(PlugChangeListener mPlugChangeListener) {
        this.mPlugChangeListener = mPlugChangeListener;
    }
    public void toggle(){
        if(getState()==PlugState.OFF)
            setState(PlugState.ON);
        else
            setState(PlugState.OFF);
    }
}
