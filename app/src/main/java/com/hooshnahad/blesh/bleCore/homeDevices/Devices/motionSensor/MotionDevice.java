package com.hooshnahad.blesh.bleCore.homeDevices.Devices.motionSensor;

import com.hooshnahad.blesh.bleCore.homeDevices.Devices.BleDevice;
import com.hooshnahad.blesh.bleCore.homeDevices.Devices.DeviceType;
import com.hooshnahad.blesh.bleCore.homeDevices.bleManager.BleConnectionState;

/**
 * Created by Freez on 7/20/2016.
 */
public class MotionDevice extends BleDevice implements BleDevice.BleConnectionListener,BleDevice.BleRssiListener {

    MotionChangeListener motionChangeListener;
    boolean motion;


    public MotionDevice(String name,String address,MotionChangeListener motionChangeListener) {
        this.motionChangeListener = motionChangeListener;
        init(name,address);
    }
    public MotionDevice(String name,String address) {
        init(name,address);
    }
    public void init(String name,String address){
        this.setDeviceType(DeviceType.MOTION);
        this.setName(name);
        this.setAddress(address);
        this.setBleRssiListener(this);
        this.setBleConnectionListener(this);
        this.setMotion(false);
    }

    public interface MotionChangeListener{
        void onMotionStateChange(String address,boolean motion);
        void onMotionRssiChange(String address,int rssi);
        void onMotionConnectionChange(String address,BleConnectionState connectionState);
    }

    @Override
    public void onBleConnectionChanged(String address, BleConnectionState connectionState) {
        if(address.equals(this.getAddress()))
            motionChangeListener.onMotionConnectionChange(address,connectionState);
    }

    @Override
    public void onChangeRssi(String address, int rssi) {
        if(address.equals(this.getAddress()))
            motionChangeListener.onMotionRssiChange(address, rssi);
    }


    public void setMotionChangeListener(MotionChangeListener motionChangeListener) {
        this.motionChangeListener = motionChangeListener;
    }

    public boolean isMotion() {
        return motion;
    }

    public void setMotion(boolean motion) {
        this.motion = motion;
        if(motionChangeListener!=null)
            motionChangeListener.onMotionStateChange(getAddress(),motion);
    }

    // TODO: 7/23/2016  clear this function
    public void tempToggle(){
        if(isMotion()){
            setMotion(false);
        }else{
            setMotion(true);
        }
    }
}
