package com.hooshnahad.blesh.bleCore.homeDevices.Devices.doorSensor;

import com.hooshnahad.blesh.bleCore.homeDevices.Devices.BleDevice;
import com.hooshnahad.blesh.bleCore.homeDevices.Devices.DeviceType;
import com.hooshnahad.blesh.bleCore.homeDevices.bleManager.BleConnectionState;

/**
 * Created by Freez on 7/20/2016.
 */
public class DoorSensorDevice extends BleDevice implements BleDevice.BleConnectionListener,BleDevice.BleRssiListener{

    DoorState doorState;
    BleDoorChangeListener mBleDoorChangeListener;

    public DoorSensorDevice(String name,String address) {
        init(name,address);
    }

    public DoorSensorDevice(String name,String address,BleDoorChangeListener mBleDoorChangeListener) {
        init(name,address);
        this.mBleDoorChangeListener = mBleDoorChangeListener;
    }
    private void init(String name,String address){
        this.setDeviceType(DeviceType.COOLER);
        this.setName(name);
        this.setAddress(address);
        this.setBleConnectionListener(this);
        this.setBleRssiListener(this);
        setDoorState(DoorState.CLOSE);
    }

    @Override
    public void onBleConnectionChanged(String address, BleConnectionState connectionState) {
        if(address.equals(this.getAddress()))
            mBleDoorChangeListener.onDoorConnectionStateChange(this.getAddress(), connectionState);

    }

    @Override
    public void onChangeRssi(String address, int rssi) {
        if(address.equals(this.getAddress()))
            mBleDoorChangeListener.onDoorRssiChange(this.getAddress(),rssi);

    }

    public interface BleDoorChangeListener{
        public void onDoorStateChange(String address,DoorState doorState);
        public void onDoorConnectionStateChange(String address,BleConnectionState connectionState);
        public void onDoorRssiChange(String address,int rssi);
    }

    public DoorState getDoorState() {
        return doorState;
    }

    public void setDoorState(DoorState doorState) {
        this.doorState = doorState;
        if(mBleDoorChangeListener!=null)
            mBleDoorChangeListener.onDoorStateChange(this.getAddress(),doorState);
//        else
//            Log.i("setDoorState","listener null");

    }

    public void setBleDoorChangeListener(BleDoorChangeListener mBleDoorChangeListener) {
//        Log.i("BleDoorChangeListener","ok");
        this.mBleDoorChangeListener = mBleDoorChangeListener;
    }

    // TODO: 7/23/2016  delete this function
    public void tempToggle(){
        if(doorState==DoorState.OPEN)
            setDoorState(DoorState.CLOSE);
        else
            setDoorState(DoorState.OPEN);

    }
}
