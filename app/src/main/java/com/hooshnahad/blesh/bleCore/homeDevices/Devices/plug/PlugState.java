package com.hooshnahad.blesh.bleCore.homeDevices.Devices.plug;

/**
 * Created by Freez on 7/23/2016.
 */
public enum PlugState {
    ON(1),OFF(0);
    private final int value;
    private PlugState(int value){
        this.value = value;
    }
    public int getValue(){
        return this.value;
    }
}
