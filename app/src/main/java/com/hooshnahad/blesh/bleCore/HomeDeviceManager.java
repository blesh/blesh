package com.hooshnahad.blesh.bleCore;

import android.content.Context;
import android.os.Handler;


import com.hooshnahad.blesh.bleCore.homeDevices.Devices.BleDevice;
import com.hooshnahad.blesh.bleCore.homeDevices.Devices.doorSensor.DoorSensorDevice;
import com.hooshnahad.blesh.bleCore.homeDevices.Devices.doublePoleSwitch.DoublePoleSwitchDevice;
import com.hooshnahad.blesh.bleCore.homeDevices.Devices.evaporativeCooler.EvaporativeCoolerDevice;
import com.hooshnahad.blesh.bleCore.homeDevices.Devices.motionSensor.MotionDevice;
import com.hooshnahad.blesh.bleCore.homeDevices.Devices.plug.PlugDevice;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Freez on 7/20/2016.
 */
public class HomeDeviceManager {
    private List<BleDevice> availableBleDevice;
    private List<BleDevice> pairedBleDevice;

    private List<PlugDevice> plugDeviceList;
    private List<DoublePoleSwitchDevice> doublePoleSwitchDeviceList;
    private List<DoorSensorDevice> doorSensorDeviceList;
    private List<MotionDevice> motionDeviceList;
    private List<EvaporativeCoolerDevice> evaporativeCoolerDeviceList;
    private static HomeDeviceManager instance;
    private HomeDeviceManager(Context context){
        plugDeviceList = new ArrayList<PlugDevice>();
        doublePoleSwitchDeviceList = new ArrayList<DoublePoleSwitchDevice>();
        doorSensorDeviceList = new ArrayList<DoorSensorDevice>();
        motionDeviceList = new ArrayList<MotionDevice>();
        evaporativeCoolerDeviceList = new ArrayList<EvaporativeCoolerDevice>();
        // TODO: 7/23/2016 test case :
        doorSensorDeviceList.add(new DoorSensorDevice("door","123456789"));
        plugDeviceList.add(new PlugDevice("plug", "6484841564"));
        doublePoleSwitchDeviceList.add(new DoublePoleSwitchDevice("dpst","35158548"));
        motionDeviceList.add(new MotionDevice("motion","78785452968"));
        evaporativeCoolerDeviceList.add(new EvaporativeCoolerDevice("cooler","78754887538"));
        mHandler = new Handler();

    }

    public static HomeDeviceManager getInstance(Context context) {
        if( instance == null)
            instance = new HomeDeviceManager(context);
        return instance;
    }

    public List<PlugDevice> getPlugDevices() {
        return plugDeviceList;
    }

    public List<DoublePoleSwitchDevice> getDoublePoleSwitchDevices() {
        return doublePoleSwitchDeviceList;
    }

    public List<DoorSensorDevice> getDoorSensorDeviceList() {
        return doorSensorDeviceList;
    }

    public List<MotionDevice> getMotionDevices() {
        return motionDeviceList;
    }


    public List<EvaporativeCoolerDevice> getEvaporativeCoolerDeviceList() {
        return evaporativeCoolerDeviceList;
    }
    public void setDoorsListener(DoorSensorDevice.BleDoorChangeListener doorsListener){
//        Log.i("HomeDeviceManager","door "+doorSensorDeviceList.size());
        for(int i=0;i<doorSensorDeviceList.size();i++){
            doorSensorDeviceList.get(i).setBleDoorChangeListener(doorsListener);
        }
    }
    public void setCoolerListener(EvaporativeCoolerDevice.EvaporativeCoolerChangeListener coolerListener){
//        Log.i("HomeDeviceManager","cooler "+evaporativeCoolerDeviceList.size());
        for(int i=0;i<evaporativeCoolerDeviceList.size();i++){
            evaporativeCoolerDeviceList.get(i).setEvaporativeCoolerChangeListener(coolerListener);
        }
    }
    public void setPlugListener(PlugDevice.PlugChangeListener plugListener){
//        Log.i("HomeDeviceManager","plug "+plugDeviceList.size());
        for(int i=0;i<plugDeviceList.size();i++){
            plugDeviceList.get(i).setPlugChangeListener(plugListener);
        }
    }
    public void setDoublePoleSwitchListener(DoublePoleSwitchDevice.DoublePoleSwitchChangeListener dpstListener){
//        Log.i("HomeDeviceManager","dpst "+doublePoleSwitchDeviceList.size());
        for(int i=0;i<doublePoleSwitchDeviceList.size();i++){
            doublePoleSwitchDeviceList.get(i).setDoublePoleSwitchListener(dpstListener);
        }
    }
    public void setMotionListener(MotionDevice.MotionChangeListener motionListener){
//        Log.i("HomeDeviceManager","motion "+motionDeviceList.size());
        for(int i=0;i<motionDeviceList.size();i++){
            motionDeviceList.get(i).setMotionChangeListener(motionListener);
        }
    }

    /*
    * write test case
    * */
    private int mInterval = 5000; // 5 seconds by default, can be changed later
    private Handler mHandler;
    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                Random rand = new Random();
                int  n = rand.nextInt(5) + 1;
                if(n==1){
                    doorSensorDeviceList.get(0).tempToggle(); //this function can change value of mInterval.
                }
                else if(n==2){
                    plugDeviceList.get(0).toggle();
                }
                else if(n==3){
                    doublePoleSwitchDeviceList.get(0).toggleFirstPol();
                    doublePoleSwitchDeviceList.get(0).toggleSecondPol();
                }
                else if(n==4){
                    motionDeviceList.get(0).tempToggle();
                }
                else if(n==5){
                    evaporativeCoolerDeviceList.get(0).tempTogglePomp();
                    evaporativeCoolerDeviceList.get(0).tempToggleEngine();
                }


            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    public void startRepeatingTask() {
        mStatusChecker.run();
    }

    public void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }

    public List<BleDevice> getAvailableBleDevice() {
        availableBleDevice = new ArrayList<>();
        // TODO: 7/24/2016 change this
        for(int i=0;i<plugDeviceList.size();i++){
            availableBleDevice.add(plugDeviceList.get(i));
        }
        for(int i=0;i<doublePoleSwitchDeviceList.size();i++){
            availableBleDevice.add(doublePoleSwitchDeviceList.get(i));
        }
        for(int i=0;i<doublePoleSwitchDeviceList.size();i++){
            availableBleDevice.add(doublePoleSwitchDeviceList.get(i));
        }
        for(int i=0;i<motionDeviceList.size();i++){
            availableBleDevice.add(motionDeviceList.get(i));
        }
        for(int i=0;i<evaporativeCoolerDeviceList.size();i++){
            availableBleDevice.add(evaporativeCoolerDeviceList.get(i));
        }
        return availableBleDevice;
    }

    public List<BleDevice> getPairedBleDevice() {
        pairedBleDevice = new ArrayList<>();
        // TODO: 7/24/2016 change this
        for(int i=0;i<plugDeviceList.size();i++){
            pairedBleDevice.add(plugDeviceList.get(i));
        }
        for(int i=0;i<doublePoleSwitchDeviceList.size();i++){
            pairedBleDevice.add(doublePoleSwitchDeviceList.get(i));
        }
        for(int i=0;i<doublePoleSwitchDeviceList.size();i++){
            pairedBleDevice.add(doublePoleSwitchDeviceList.get(i));
        }
        for(int i=0;i<motionDeviceList.size();i++){
            pairedBleDevice.add(motionDeviceList.get(i));
        }
        for(int i=0;i<evaporativeCoolerDeviceList.size();i++){
            pairedBleDevice.add(evaporativeCoolerDeviceList.get(i));
        }
        return pairedBleDevice;
    }
}
