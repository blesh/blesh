package com.hooshnahad.blesh;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hooshnahad.blesh.bleCore.homeDevices.Devices.doorSensor.DoorSensorDevice;
import com.hooshnahad.blesh.bleCore.homeDevices.Devices.doorSensor.DoorState;
import com.hooshnahad.blesh.bleCore.homeDevices.Devices.plug.PlugDevice;
import com.hooshnahad.blesh.bleCore.homeDevices.Devices.plug.PlugState;
import com.hooshnahad.blesh.bleCore.homeDevices.HomeDeviceManager;
import com.hooshnahad.blesh.bleCore.homeDevices.bleManager.BleConnectionState;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener
                    ,DoorSensorDevice.BleDoorChangeListener
                    ,PlugDevice.PlugChangeListener{

    TextView doorStateTv,doorRssiTv;
    TextView plugStateTv,plugRssiTv;
    HomeDeviceManager homeDeviceManager;
    Button doorToggleBtn;
    Button plugToggleBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        homeDeviceManager = HomeDeviceManager.getInstance(this);

        //===========DOOR=========================
        doorRssiTv = (TextView)findViewById(R.id.door_rssi);
        doorStateTv = (TextView)findViewById(R.id.door_state);
        doorToggleBtn = (Button) findViewById(R.id.toggle_btn);
        doorToggleBtn.setOnClickListener(this);


        homeDeviceManager.setDoorsListener(this);
        //===============PLUG==============================
        plugRssiTv = (TextView)findViewById(R.id.plug_rssi);
        plugStateTv = (TextView)findViewById(R.id.plug_state);
        plugToggleBtn = (Button) findViewById(R.id.toggle_plug);
        plugToggleBtn.setOnClickListener(this);
        homeDeviceManager.setPlugListener(this);

        //================================================
        homeDeviceManager.startRepeatingTask();

    }

    @Override
    protected void onPause() {
        super.onPause();
        homeDeviceManager.startRepeatingTask();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.toggle_btn:
                homeDeviceManager.getDoorSensorDeviceList().get(0).tempToggle();
                break;
        }
    }

    @Override
    public void onDoorStateChange(String address, DoorState doorState) {
        Log.d("onDoorStateChange", doorState.toString());
        doorStateTv.setText("Door State: "+doorState.toString());
    }

    @Override
    public void onDoorConnectionStateChange(String address, BleConnectionState connectionState) {

    }

    @Override
    public void onDoorRssiChange(String address, int rssi) {
        doorRssiTv.setText("Door RSSI: " + rssi);
    }


    @Override
    public void onPlugStateChange(String address, PlugState state) {
        plugStateTv.setText("Plug State: "+state.toString());
    }

    @Override
    public void onPlugConnectionChange(String address, BleConnectionState connectionState) {

    }

    @Override
    public void onPlugRssiChange(String address, int rssi) {
        plugRssiTv.setText("Plug RSSI: " + rssi);
    }
}
